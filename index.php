<?php require "vendor/autoload.php";
require __DIR__ . '/vendor/autoload.php';
use Josantonius\Session\Session;

use \Tamtamchik\SimpleFlash\Flash;
use \Respect\Validation\Exceptions\NestedValidationException;


// permet de ce créer une variable de session qui circulera dans tous le site !!
Session::init();
if(Session::get('auth') == null){
    Session::set('auth', false);
}


// on indique a flight que l'on vas utiliser twig
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);



// flight s'inscrit ici comme une extension
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function($string){
        return renderHTMLFromMarkdown($string);
    }));
    $twig->addFilter(new Twig_Filter('date_format', function($string){
        $string = str_replace("/", '-', $string);
        $dates = explode("-", $string);
        return sprintf("%s-%s-%s", $dates[0], $dates[1], $dates[2]);
    }));
    $twig->addFunction(new Twig_Function('messages', function(){
        return Flash::display();
    }));
});



// On vient réaliser une connection à la base de donnée jeux.sqlite3 !!
Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:jeux.sqlite3');
});


//----------------------------------------------------------------------------
//                          ROUTAGE
//----------------------------------------------------------------------------


// on indique à l'utilisateur qu'il c'est trompé en tapant l'adresse dans l'url
Flight::map('notFound', function(){
    // Display custom 404 page
    Flight::view()->display('mauvais_url.twig');
});




// on demande à l'utilisateur de ce connecter !!
Flight::route('/connection', function(){
    if(Flight::request()->method == 'POST'){
        $utilisateur = Model::factory('Utilisateur')
                ->where('email', Flight::request()->data['email'])
                ->find_one();
        if($utilisateur){
            Session::set('auth', $utilisateur->id);
            Session::set('auth_user', $utilisateur);
            Flight::redirect('/');
        }else{
            echo 'Invalid user';
            Flight::redirect('/connection');
        }
    }else{
        Flight::view()->display('connection.twig');
    }
});

// si l'utilisateur n'as pas de compte alors il peut en créer une grace à cette route
Flight::route('/creer_compte', function(){
    if(Flight::request()->method == 'POST'){
        $jeux = Model::factory('Utilisateur')->create();
        $jeux->email = Flight::request()->data->email;
        $jeux->mdp = Flight::request()->data->mdp;
        $jeux->save();
        Flight::redirect('/connection');
    }
    $data=[
        'utilisateur' => Utilisateur::find_many(),               
    ];
    Flight::view()->display('creer_compte.twig',$data);
});


// accueil de notre site web !!
Flight::route('/', function(){
    $session= [
        'auth_user' => Session::get('auth_user'),
        'auth' => Session::get('auth'),
    ]; 
    Flight::view()->display('accueil.twig',$session);
});


// affiche tous les jeux de la base de donnée.
Flight::route('/jeux/tous', function(){
    $session= [
        'auth_user' => Session::get('auth_user'),
        'auth' => Session::get('auth'),
        'jeux' => Jeu::find_many()
    ]; 
 
    Flight::view()->display('jeux_tous.twig',$session);
});





// permet d'afficher les détails d'un jeu
Flight::route('/jeux/details/@id', function($id){
    $jeux_details =  Model::factory('Jeu')
                ->where('id',$id)
                ->find_one();

 
    $session= [
        'auth_user' => Session::get('auth_user'),
        'auth' => Session::get('auth'),
        'jeu' => $jeux_details
    ]; 
      Flight::view()->display('jeux_details.twig',$session);
});




// on peut ajouter des jeux video que l'on possède ou que l'on veut retrouver grace à notre 
// variable de session nous possèdont l'id de l'utilisateur.
Flight::route('/jeux/comte(/@id)',function($id){
    if($id){
        $jeux = Model::factory('Utilisateur_jeux')->create();
        $jeux->id_utilisateur = Session::get('auth');
        $jeux->id_jeux= $id; 
        $jeux->save();
    }
    $id_session =Session::get('auth');   

    $session= [
        'auth_user'=>Session::get('auth_user'),
        'jeux'=>Model::factory('Utilisateur_jeux')
                ->where('id_utilisateur',$id_session)
                ->find_many(), 
    ];    
    Flight::view()->display('jeux_comte.twig',$session);
});

//----------------------------------------------------------------------
//                      QUESTIONNAIRE
//----------------------------------------------------------------------

Flight::route('/formulaire(/@id)', function($id){ 
    if($id){
        $jeux = Model::factory('Jeu')->find_one($id);  
    }else{
        $jeux = Model::factory('Jeu')->create();
        $jeux->descriptif = NULL;
        $jeux->photo = NULL;
        $jeux->nom = NULL;
        $jeux->date = NULL;
        $jeux->id_support = NULL;
        $jeux->id_editeur = NULL;
        $jeux->id_types = NULL;
    }
    if(Flight::request()->method == 'POST'){
        if(Flight::request()->files['photo']  && Flight::request()->files['photo']['tmp_name']){
            $avatar_file = convertFileToBlob(Flight::request()->files['photo']['tmp_name']);
            $jeux->photo = $avatar_file;
        }
        $jeux->descriptif = Flight::request()->data->descriptif;
        $jeux->nom = Flight::request()->data->nom;
        $jeux->date = Flight::request()->data->date;
        $jeux->id_support = Flight::request()->data->id_support;
        $jeux->id_editeur= Flight::request()->data->id_editeur;
        $jeux->id_types = Flight::request()->data->id_types;
        if(validateJeux($jeux)){
            if($id){
                echo"Le jeux est enregistré";
            }else{
                echo"le jeux a été crée";
            }
            $jeux->save();
            Flight::redirect('/');
        }
        else{
            echo"Ooops il y a une erreur dans votre formulaire ! N'oubliez pas de rajouter votre image !";
            $data = [
                'jeux' => $jeux,
                'supports'=>Support1::find_many(),
                'editeurs'=>Editeur::find_many(),
                'types'=>Types::find_many(),
                'auth_user' => Session::get('auth_user'),
                'auth' => Session::get('auth')
            ];
            Flight::view()->display('form.twig',$data);
        }
    }
    else{
        $data = [
            'jeux' => $jeux,
            'supports'=>Support1::find_many(),
            'editeurs'=>Editeur::find_many(),
            'types'=>Types::find_many(),
            'auth_user' => Session::get('auth_user'),
            'auth' => Session::get('auth')
        ];
        Flight::view()->display('form.twig',$data);
    }
    
});


// supprimer un élément de notre base de donnée
Flight::route('/jeux/delete/@id', function($id){
    $jeux= Model::factory('Jeu')->find_one($id);
    echo"Le jeu à été suprimer";
    $utilisateur = Model::factory('Utilisateur_jeux')->where('id_jeux', $id)->find_one();
    if($utilisateur==NULL){
        $jeux->delete();
    }else {
        $jeux->delete();
        $utilisateur->delete();
        Flight::redirect('/jeux/tous');
    }         
    Flight::redirect('/');
});

Flight::route('/jeux/delete/vosjeux/@id', function($id){
    $jeux= Model::factory('Utilisateur_jeux')->find_one($id);
    echo"Le jeu à été suprimer";
    $jeux->delete();
    Flight::redirect('/');
});




Flight::route('/deconnexion',function(){
    $id=0;
    Session::set('auth_user',$id);
    $session=[
        'auth_user'=> Session::get('auth_user'),
    ];
    Flight::view()->display('accueil.twig',$session);
});
Flight::start();
