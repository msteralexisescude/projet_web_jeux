<?php

class Utilisateur_jeux extends Model {
    public static $_table='utilisateur_jeux';

    public function utilisateur()
    {
        return $this->belongs_to('Utilisateur', 'id_utilisateur')->find_one();
    }

    
    public function jeux()
    {
        return $this->belongs_to('Jeu', 'id_jeux')->find_one();
    }
   
}


class Utilisateur extends Model {
    public static $_table='utilisateur';
}


class Support1 extends Model {
    public static $_table = 'support'; 

}

class Editeur extends Model { 
    public static $_table = 'editeur'; 
}

class Types extends Model { 
    public static $_table = 'types'; 

}


class Jeu extends Model { 
    public static $_table = 'jeux'; 


    public function support()
    {
        return $this->belongs_to('Support1', 'id_support')->find_one();

    }

    public function editeur()
    {
        return $this->belongs_to('Editeur', 'id_editeur')->find_one();
 
    }

    public function types()
    {
        return $this->belongs_to('Types', 'id_types')->find_one();
 
    }

     
    public function avatar_b64()
    {
        return sprintf("data:image/jpeg;base64,%s", base64_encode($this->photo));
    }


    
}
