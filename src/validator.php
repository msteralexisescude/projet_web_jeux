<?php

use \Tamtamchik\SimpleFlash\Flash;
use Respect\Validation\Validator as v;



function isDate($value) 
{
    if (!$value) {
        return false;
    }

    try {
        new \DateTime($value);
        return true;
    } catch (\Exception $e) {
        return false;
    }
}

function validateJeux($jeux){

    if(is_null($jeux->nom) or strlen($jeux->nom) < 3){
        echo"Le nom dois avoir 3 caractère minimun";
        return false;
    }

    if(!isDate($jeux->date)){
        echo"La date n'est pas corecttement saisie.";
        return false;
    }

    if(is_null($jeux->descriptif) or strlen($jeux->descriptif) < 10){
        echo"Ledescriptif dois avoir 10 caractère minimun";
        return false;
    }
  

    return true;
}