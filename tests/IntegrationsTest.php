<?php
require_once 'vendor/autoload.php';


class PagesIntegrationTest extends IntegrationTest{

    public function test_index()
    {  
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Connectez-vous pour accéder au site !", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_jeux_tous()
    {  
        $response = $this->make_request("GET", "/jeux/tous");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Merci de votre visite ! n'oubliez pas de vous déconnecter :)", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    
    public function test_jeux_comte()
    {  
        $response = $this->make_request("GET", "/jeux/comte");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Votre liste de jeux:", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    
      
    public function test_erreur_url()
    {  
        $response = $this->make_request("GET", "/erreurrr");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Erreur d'adresse, vous vous êtes trompé dans l'url !!", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }


    
    
}