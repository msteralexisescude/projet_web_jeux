<?php

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;


class UnitariesTest extends TestCase {


    public function test_validateJeux(){
        $jeux = Model::factory('Jeu')->create();
        $jeux->descriptif = 'ceci es un descriptif de qualité';
        $jeux->photo = NULL;
        $jeux->nom = 'call of duty';
        $jeux->date = 2018-04-18;
        $jeux->id_support = NULL;
        $jeux->id_editeur = NULL;
        $jeux->id_types = NULL;
        $this->assertEquals(true, validateJeux($jeux));
    }

    public function test_isDat(){
        $date=2018-04-18;
        $this->assertEquals(true, isDate($date));
        
    }
}